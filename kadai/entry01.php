<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>新規登録</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div style="text-align:center"><h1>新規登録</h1></div>
    <form method="POST" action="entry02.php">
      <div style="text-align:center"><a href="index.php">トップ画面</a></div>
      <table border="5"align="center">
        <tr>
          <td>名前:<input type="text" name="title" size="22" maxlength="30"  /></td>
        </tr>

        <tr>
          <td>性別:<input type="radio" name="seibetu" value="男" checked />男
                  <input type="radio" name="seibetu" value="女" />女</td>
        </tr>

        <tr>
          <td>年齢:<input type="text" name="tosi" size="5" maxlength="15"  /></td>
        </tr>

        <tr>
          <td>出身地:<select name="tiiki">
              <!-- <option value="" selected　>地域</option>
              <option value="香川">香川</option>
              <option value="高知">高知</option>
              <option value="愛媛">愛媛</option>
              <option value="徳島">徳島</option> -->
             <option value="" selected>都道府県</option>
             <option value="北海道">北海道</option>
             <option value="青森県">青森県</option>
             <option value="岩手県">岩手県</option>
             <option value="宮城県">宮城県</option>
             <option value="秋田県">秋田県</option>
             <option value="山形県">山形県</option>
             <option value="福島県">福島県</option>
             <option value="茨城県">茨城県</option>
             <option value="栃木県">栃木県</option>
             <option value="新潟県">群馬県</option>
             <option value="埼玉県">埼玉県</option>
             <option value="千葉県">千葉県</option>
             <option value="東京都">東京都</option>
             <option value='神奈川県'>神奈川県</option>
             <option value='新潟県'>新潟県</option>
             <option value='富山県'>富山県</option>
             <option value='石川県'>石川県</option>
             <option value='福井県'>福井県</option>
             <option value='山梨県'>山梨県</option>
             <option value='長野県'>長野県</option>
             <option value='岐阜県'>岐阜県</option>
             <option value='静岡県'>静岡県</option>
             <option value='愛知県'>愛知県</option>
             <option value='三重県'>三重県</option>
             <option value='滋賀県'>滋賀県</option>
             <option value='京都府'>京都府</option>
             <option value='大阪府'>大阪府</option>
             <option value='兵庫県'>兵庫県</option>
             <option value='奈良県'>奈良県</option>
             <option value='和歌山県'>和歌山県</option>
             <option value='鳥取県'>鳥取県</option>
             <option value='島根県'>島根県</option>
             <option value='岡山県'>岡山県</option>
             <option value='広島県'>広島県</option>
             <option value='山口県'>山口県</option>
             <option value='徳島県'>徳島県</option>
             <option value='香川県'>香川県</option>
             <option value='愛媛県'>愛媛県</option>
             <option value='高知県'>高知県</option>
             <option value='福岡県'>福岡県</option>
             <option value='佐賀県'>佐賀県</option>
             <option value='長崎県'>長崎県</option>
             <option value='熊本県'>熊本県</option>
             <option value='大分県'>大分県</option>
             <option value='宮崎県'>宮崎県</option>
             <option value='鹿児島県'>鹿児島県</option>
             <option value='沖縄県'>沖縄県</option>
            </select>
            <td>
        </tr>

        <tr>
          <td>部署:<input type="radio" name="busyo" value="1" checked/>第一事業部
                   <input type="radio" name="busyo" value="2" />第二事業部
                   <input type="radio" name="busyo" value="3" />営業
                   <input type="radio" name="busyo" value="4" />総務
                   <input type="radio" name="busyo" value="5" />人事
          </td>
         </tr>

         <tr>
           <td>
            役職:<input type="radio" name="yakusyoku" value="1" checked/>営業部長
                 <input type="radio" name="yakusyoku" value="2" />部長
                 <input type="radio" name="yakusyoku" value="3" />チームリーダー
                 <input type="radio" name="yakusyoku" value="4" />リーダー
                 <input type="radio" name="yakusyoku" value="5" />メンバー
            </td>
          </tr>
        </table>
      <div style="text-align:center"><input type="submit" name="btn2" value="登録" ></div>
      <div style="text-align:center"><input type="reset" name="btn1" value="リセット" ></div>

      </body>
    </html>
